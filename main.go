package main

import (
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/sdl_gfx"
	"math"
	"runtime"
)

const (
	windowWidth  = 500
	windowHeight = 500
	framerate    = 60
	windowTitle  = "Sierpinski Triangle"
)

var (
	window   *sdl.Window
	renderer *sdl.Renderer
)

type tri struct {
	x1, y1, x2, y2, x3, y3 float64
}

func init() {
	runtime.LockOSThread()
}

func main() {
	fmt.Println("vim-go")

	if err := sdl.Init(sdl.INIT_VIDEO); err != nil {
		panic(err)
	}

	createWindow()

	//initTri := tri{0, 500, 500, 500, 250, 500 - (500 / 2 * math.Sqrt(3))}
	len := float64(windowWidth)
	initTri := createTri(windowWidth/2, windowHeight/2, len)
	for {
		/*
			if len >= windowWidth*10 {
				len = windowWidth * 5
			} else {
				//initTri = createTri(windowWidth/2, windowHeight, len)
				updateTri(windowWidth/4, windowHeight/2, len, &initTri)
			}*/
		updateTri(windowWidth/2.3, windowHeight/2, len, &initTri)
		renderer.Clear()
		renderer.SetDrawColor(0xff, 0xff, 0xff, 0xff)
		renderer.FillRect(&sdl.Rect{0, 0, windowWidth, windowHeight})
		drawTri(initTri, true)
		drawTri(innerNegativeTriangle(initTri), false)
		drawInner(initTri)
		renderer.Present()
		sdl.Delay(1000 / 30)
		if checkZoom(&initTri) {
			len = len / 2
		}
		len += len / 40
	}

}

func createWindow() {
	var err error
	flags := sdl.WINDOW_OPENGL

	window, renderer, err = sdl.CreateWindowAndRenderer(windowWidth, windowHeight, uint32(flags))
	if err != nil {
		panic(err)
	}

	window.SetTitle(windowTitle)
}

func createTri(x, y, len float64) tri {
	return tri{x - len/2, y + ((len/2)*math.Sqrt(3))/2, x + len/2, y + ((len/2)*math.Sqrt(3))/2, x, y - ((len/2)*math.Sqrt(3))/2}
}

func updateTri(x, y, len float64, t *tri) {
	size := t.x2 - t.x1
	height := t.y1 - t.y3
	calc := (len / 2) * math.Sqrt(3)

	t.x1 = x - (len * ((x - t.x1) / size))
	t.x2 = x + (len * ((t.x2 - x) / size))

	if t.x3 < x {
		t.x3 = x - (len * (x - t.x3) / size)
	} else {
		t.x3 = x + (len * (t.x3 - x) / size)
	}

	t.y1 = y + (calc * ((t.y1 - y) / height))
	t.y2 = y + (calc * ((t.y2 - y) / height))
	t.y3 = y - (calc * ((y - t.y3) / height))
}

func drawTri(t tri, on bool) {
	var c sdl.Color
	c = sdl.Color{0xff, 0xff, 0xff, 0xff}
	if on {
		c = sdl.Color{0x00, 0x00, 0x00, 0xff}
	}
	gfx.FilledTrigonColor(renderer, int(t.x1), int(t.y1), int(t.x2), int(t.y2), int(t.x3), int(t.y3), c)
}

func drawInner(t tri) {
	if math.Abs(t.x1-t.x2) < 100 || t.x1 > windowWidth || t.x2 < 0 || t.y3 > windowHeight || t.y1 < 0 {
		return
	}
	outer := outerPositiveTriangles(t, innerNegativeTriangle(t))
	for _, tri := range outer {
		drawTri(innerNegativeTriangle(tri), false)
		drawInner(tri)
	}
}

func innerNegativeTriangle(t tri) tri {
	var newTri tri
	newTri.x1 = (math.Abs(t.x1-t.x2) / 2) + math.Min(t.x1, t.x2)
	newTri.y1 = (math.Abs(t.y1-t.y2) / 2) + math.Min(t.y1, t.y2)
	newTri.x2 = (math.Abs(t.x2-t.x3) / 2) + math.Min(t.x2, t.x3)
	newTri.y2 = (math.Abs(t.y2-t.y3) / 2) + math.Min(t.y2, t.y3)
	newTri.x3 = (math.Abs(t.x3-t.x1) / 2) + math.Min(t.x3, t.x1)
	newTri.y3 = (math.Abs(t.y3-t.y1) / 2) + math.Min(t.y3, t.y1)
	return newTri
}

func outerPositiveTriangles(pos, neg tri) []tri {
	var tri1, tri2, tri3 tri
	tri1.x1 = pos.x1
	tri1.y1 = pos.y1
	tri1.x2 = neg.x1
	tri1.y2 = neg.y1
	tri1.x3 = neg.x3
	tri1.y3 = neg.y3

	tri2.x1 = neg.x1
	tri2.y1 = neg.y1
	tri2.x2 = pos.x2
	tri2.y2 = pos.y2
	tri2.x3 = neg.x2
	tri2.y3 = neg.y2

	tri3.x1 = neg.x3
	tri3.y1 = neg.y3
	tri3.x2 = neg.x2
	tri3.y2 = neg.y2
	tri3.x3 = pos.x3
	tri3.y3 = pos.y3
	slice := make([]tri, 0, 3)
	slice = append(slice, tri1)
	slice = append(slice, tri2)
	slice = append(slice, tri3)
	return slice

}

func checkZoom(t *tri) bool {
	outer := outerPositiveTriangles(*t, innerNegativeTriangle(*t))
	for _, triangle := range outer {
		if triangle.x1 < 0 && triangle.y1 > windowHeight && triangle.x2 > windowWidth && triangle.y3 < 0 {
			t.x1 = triangle.x1
			t.x2 = triangle.x2
			t.x3 = triangle.x3
			t.y1 = triangle.y1
			t.y2 = triangle.y2
			t.y3 = triangle.y3
			fmt.Println(triangle)
			return true
		}
	}
	return false
}
